var links = document.getElementsByTagName("a");
var divs = document.getElementsByTagName("div");
var prefix = "Click this link to access ";
var item_names = new Array();
var prices = new Array();
var i, j, k;
const THRESHOD = 0.75;

// Collect all items names from the page.
// An item name includes "Click this link to access " as the prefix.
for(i=0;i<links.length;i++){
    if(links[i].title.indexOf(prefix) != -1){
	item_names.push(links[i].title.substr(prefix.length));
    }
}

// Collect all item prices from the page.
// An item price is include in a div whose class is "g-b" (if on sale) or "g-b bidsold" (if sold, regardless of whether BIN or not).
for(i=0;i<divs.length;i++){
    if(divs[i].className == "g-b bidsold" || divs[i].className == "g-b"){
	divs[i].innerHTML.match(new RegExp("[^0-9]*([0-9,]+)[^0-9]*"));
	prices.push(RegExp.$1);
    }
}

var deduplicate = function(array){
    var ans = new Array();
    var p, q;

    for(p=0;p<array.length;p++){
	var contained = false;

	for(q=0;q<ans.length;q++)
	    if(ans[q] == array[p])
		contained = true;

	if(!contained)
	    ans.push(array[p]);
    }

    return ans;
}

for(i=0;i<item_names.length;i++){
    for(j=i+1;j<item_names.length;j++){
	var inner_product = 0;
	var cosine_similarity;
	var words_i = deduplicate(item_names[i].split(" "));
	var words_j = deduplicate(item_names[j].split(" "));

	// the inner product of item_names[i] and item_names[j] is equal to 
	// the number of words_i[k] (k=0..length) contained in items_names[j]
	// because all the vector elements associated with each word is 1 or 0
	for(k=0;k<words_i.length;k++){
	    if(item_names[j].indexOf(words_i[k]) != -1)
		inner_product++;
	}

	// words_*.length is equal to Sum_{w in words_i} words_i[w]^2
	// because a word is included only once (thus words_i[w] is always 1)
	cosine_similarity = inner_product / (Math.sqrt(words_i.length) * Math.sqrt(words_i.length));
	
	if(cosine_similarity > THRESHOD){
	    var result = document.createElement("div");
	    result.innerHTML = item_names[i] + ": " + prices[i] + " ##### " + item_names[j] + ": " + prices[j];
	    document.body.insertBefore(result, document.body.firstChild);
	}
    }
}

